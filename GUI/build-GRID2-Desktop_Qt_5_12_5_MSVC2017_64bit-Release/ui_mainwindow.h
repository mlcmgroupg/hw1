/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QWidget *gridLayoutWidget;
    QGridLayout *baseGridLayout;
    QLabel *label;
    QPushButton *setGrid_button;
    QRadioButton *Task1_radio;
    QRadioButton *Task2_radio;
    QTextBrowser *textBrowser;
    QTextBrowser *textBrowser_2;
    QTextBrowser *textBrowser_3;
    QTextBrowser *textBrowser_4;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1255, 735);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayoutWidget = new QWidget(centralwidget);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(0, 0, 1031, 691));
        baseGridLayout = new QGridLayout(gridLayoutWidget);
        baseGridLayout->setObjectName(QString::fromUtf8("baseGridLayout"));
        baseGridLayout->setSizeConstraint(QLayout::SetMinimumSize);
        baseGridLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(1090, 10, 81, 31));
        setGrid_button = new QPushButton(centralwidget);
        setGrid_button->setObjectName(QString::fromUtf8("setGrid_button"));
        setGrid_button->setGeometry(QRect(1040, 110, 171, 51));
        Task1_radio = new QRadioButton(centralwidget);
        Task1_radio->setObjectName(QString::fromUtf8("Task1_radio"));
        Task1_radio->setGeometry(QRect(1050, 40, 141, 31));
        Task2_radio = new QRadioButton(centralwidget);
        Task2_radio->setObjectName(QString::fromUtf8("Task2_radio"));
        Task2_radio->setGeometry(QRect(1050, 60, 141, 31));
        textBrowser = new QTextBrowser(centralwidget);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setGeometry(QRect(1040, 180, 71, 51));
        textBrowser->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 0);"));
        textBrowser_2 = new QTextBrowser(centralwidget);
        textBrowser_2->setObjectName(QString::fromUtf8("textBrowser_2"));
        textBrowser_2->setGeometry(QRect(1040, 240, 71, 51));
        textBrowser_2->setStyleSheet(QString::fromUtf8("background-color: rgb(85, 255, 0);"));
        textBrowser_3 = new QTextBrowser(centralwidget);
        textBrowser_3->setObjectName(QString::fromUtf8("textBrowser_3"));
        textBrowser_3->setGeometry(QRect(1040, 300, 71, 51));
        textBrowser_3->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);"));
        textBrowser_4 = new QTextBrowser(centralwidget);
        textBrowser_4->setObjectName(QString::fromUtf8("textBrowser_4"));
        textBrowser_4->setGeometry(QRect(1040, 360, 71, 51));
        textBrowser_4->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 0, 0);"));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(1130, 190, 81, 21));
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(1130, 260, 61, 16));
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(1130, 320, 55, 16));
        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(1130, 370, 71, 31));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1255, 26));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        label->setText(QApplication::translate("MainWindow", "ENTER SIZE", nullptr));
        setGrid_button->setText(QApplication::translate("MainWindow", "Set Grid", nullptr));
        Task1_radio->setText(QApplication::translate("MainWindow", "Task_1 10x10 Grid", nullptr));
        Task2_radio->setText(QApplication::translate("MainWindow", "Task_2 50x50 Grid", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "EMPTY", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "PERSONS", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "TARGET", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "OBSTACLE", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
