#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.state = 0; // 0 : Uninitialized
    //w.showMaximized();
    w.show();
    return a.exec();
}
