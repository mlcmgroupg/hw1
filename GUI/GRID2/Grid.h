#include <iostream>
#include "Cell.h"
#include "Empty.h"
#include "Pedestrian.h"
#include "Obstacle.h"
#include "Target.h"
#include <vector>
#include <math.h>
#include <algorithm>
#include <QtWidgets>

using namespace std;

#pragma once
class Grid
{
private:
	int size;

	vector<Pedestrian*> pedestrians;
	vector<Target> targets;

public:
    Cell** cells;
	Grid(int);
	void add_pedestrian(int, int, int, int);
	void add_obstacle(int, int, int);
	void add_target(int, int, int);
	void add_empty(int, int);
	void add_cell(string, int, int, int);
	bool loc_is_empty(int, int, int);
	void empty_loc(int, int, int);
	void evolve_euclid();
	double get_euclidian_dist(Cell*, Cell*);
	void calculate_ped_interaction_cost();
	bool move_pedestrian(Cell*, Cell);
	void set_pedestrian(Cell*);
	int get_size();
    void print_grid();
	void print_utility_grid();
	void reset_utilities();
	void evolve_dijkstra();
};

