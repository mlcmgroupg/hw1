#pragma once
#include <iostream>
#include "Grid.h"

class Simulation
{
private:
    Grid* init_grid(int size);

public:
    Grid *grid;
    Simulation(int size);
    void run();
};

