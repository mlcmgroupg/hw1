#include "Grid.h"
#include "mainwindow.h"
#include <QtWidgets>

Grid::Grid(int size) {
	this->size = size;
	// init grid of empty cells
	this->cells = new Cell*[this->size];
	for (int i = 0; i < this->size; i++) {
		this->cells[i] = new Cell[this->size];
	}

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			this->cells[i][j] = Empty(i, j, 1);
		}
	}
}

void Grid::add_pedestrian(int loc_x, int loc_y, int obj_size, int speed) {
	//char ped_symb = (char)(this->pedestrians.size() + 1);
	char ped_symb = (this->pedestrians.size() + 1) + '0';
	Pedestrian *ped = new Pedestrian(loc_x, loc_y, obj_size, speed, ped_symb);

	for (int i = loc_x - obj_size; i <= loc_x + obj_size; i++) {
		for (int j = loc_y - obj_size; j <= loc_y + obj_size; j++) {
			this->cells[i][j] = *ped;
		}
	}
	pedestrians.push_back(ped);
}

void Grid::add_obstacle(int loc_x, int loc_y, int obj_size) {
	Obstacle obs(loc_x, loc_y, obj_size);

	for (int i = loc_x - obj_size; i <= loc_x + obj_size; i++) {
		for (int j = loc_y - obj_size; j <= loc_y + obj_size; j++) {
			this->cells[i][j] = obs;
		}
	}
}

void Grid::add_target(int loc_x, int loc_y, int obj_size) {
	Target tar(loc_x, loc_y, obj_size);

	for (int i = loc_x - obj_size; i <= loc_x + obj_size; i++) {
		for (int j = loc_y - obj_size; j <= loc_y + obj_size; j++) {
			this->cells[i][j] = tar;
		}
	}
	this->targets.push_back(tar);
}

void Grid::add_empty(int loc_x, int loc_y) {
	this->cells[loc_x][loc_y] = Empty(loc_x, loc_y, 0);
}

void Grid::add_cell(string cell_type, int loc_x, int loc_y, int obj_size) {
	if (cell_type.compare("pedestrian") == 0) {
		int speed{};
		cout << "Enter speed of pedestrian: ";
		cin >> speed;
		this->add_pedestrian(loc_x, loc_y, obj_size, speed);
	}
	else if (cell_type.compare("obstacle") == 0) {
		this->add_obstacle(loc_x, loc_y, obj_size);
	}
	else if (cell_type.compare("target") == 0) {
		this->add_target(loc_x, loc_y, obj_size);
	}
	else if (cell_type.compare("empty") == 0) {
		this->add_empty(loc_x, loc_y);
	}
	else {
		cout << "Unknown cell type" << endl;
	}
}

bool Grid::loc_is_empty(int loc_x, int loc_y, int obj_size) {
	if (loc_x - obj_size < 0 || loc_x + obj_size >= this->size || loc_y - obj_size < 0 || loc_y + obj_size >= this->size) {
		return false;
	}
	// check all the area
	for (int i = loc_x - obj_size; i <= loc_x + obj_size; i++) {
		for (int j = loc_y - obj_size; j <= loc_y + obj_size; j++) {
			if (this->cells[i][j].get_cell_char() != Empty().get_cell_char()) {
				return false;
			}
		}
	}
	return true;
}

void Grid::empty_loc(int loc_x, int loc_y, int obj_size) {
	// empties cells at given loc +- size
	for (int i = loc_x - obj_size; i <= loc_x + obj_size; i++) {
		for (int j = loc_y - obj_size; j <= loc_y + obj_size; j++) {
			this->cells[i][j] = Empty(i, j, 0);
		}
	}
}

int Grid::get_size() {
	return this->size;
}

void Grid::print_grid() {

    //MainWindow w;
    //QWidget *widget = new QWidget(w);
    //QGridLayout *layout = new QGridLayout(widget);
    /*for (int r = 0; r < 50; r++) {
        for (int c = 0; c < 50; c++) {
            QLabel *label = new QLabel("1", widget); // Text could be 1 or 0.
            if (r==c)
                label->setStyleSheet("background-color:red; color:white");
            else
                label->setStyleSheet("background-color:green; color:black");
            layout->addWidget(label, r, c);
        }
    }*/

    /*for (int r = 0; r < this->size; r++) {
            for (int c = 0; c < this->size; c++) {
                char curr = this->cells[r][c].get_cell_char();
                QLabel *label = new QLabel((QString)curr, widget);
                switch(curr)
                {
                    case 'T':
                        label->setStyleSheet("background-color:black; color:white");
                        break;
                    case 'O':
                        label->setStyleSheet("background-color:red; color:white");
                        break;
                    case '-':
                        label->setStyleSheet("background-color:yellow; color:black");

                }
                 layout->addWidget(label, r, c);
            }
        }

    widget->setLayout(layout);
    w->setCentralWidget(widget);
    QPushButton *nextB = new QPushButton("Next", widget);
    layout->addWidget(nextB, 12, 0);
    w->show();
    w->setFocus();
    //Sleep(10000);
    w->killTimer(100);
    /*for (int i = 0; i < this->size; i++) {
		for (int j = 0; j < this->size; j++) {
			cout << this->cells[i][j].get_cell_char() << " ";
		}
		cout << endl;
    }*/
}

void Grid::print_utility_grid() {
	for (int i = 0; i < this->size; i++) {
		for (int j = 0; j < this->size; j++) {
			cout << this->cells[i][j].get_utility() + this->cells[i][j].get_cost() << " ";
		}
		cout << endl;
	}
}

void Grid::evolve_euclid() {
	int x_diff{}, y_diff{};
	double dist{};
	cout << "Evolve step" << endl;
	vector<Cell> neighbors;
	// calculate utilities of grid cells based on pedestrian interaction
	this->calculate_ped_interaction_cost();
	
	// for each pedestrian check neighbors and calculate distance to target
	// check only according to center of pedestrian
	for (Pedestrian *ped : this->pedestrians) {
		neighbors.clear();
		for (int i = (ped->get_loc_x() - ped->get_size() - 1); i <= (ped->get_loc_x() + ped->get_size() + 1); i++) {
			for (int j = (ped->get_loc_y() - ped->get_size() - 1); j <= (ped->get_loc_y() + ped->get_size() + 1); j++) {
				// skip rows and cols that correspond to pedestrian itself
				if (i < 0 || i >(this->size - 1) || j < 0 || j > (this->size - 1)) {
					continue;
				}
				// checking neighbors
				// skip if neighbor is not empty
				if (!this->loc_is_empty(i, j, 0)) {
					continue;
				}
				// get euclidian distance to all targets
				for (Target tar : this->targets) {
					// iterate through all target cells
					// calculate utility on pedestrian
					dist = this->get_euclidian_dist(&tar, ped);
					// update only if utility is better than it was (to closest target)
					// so that every pedestrian moves in the direction of the closest target
					if (ped->get_utility() > dist) {
						ped->set_utility(dist);
					}
					// calculate cells utility
					dist = this->get_euclidian_dist(&tar, &this->cells[i][j]);
					// save only utilities to closest targets
					if (cells[i][j].get_utility() > dist) {
						//cout << "Setting neighbor: " << i << " " << j << " u: " << dist << endl;
						//cout << "cost: " << cells[i][j].get_cost() << endl;
						//cout << "u + cost: " << dist + cells[i][j].get_cost() << endl;
						this->cells[i][j].set_utility(dist);
					}
					// add only neighbors with better utility that pedestrian
					if (this->cells[i][j].get_utility() < ped->get_utility()) {
						neighbors.push_back(this->cells[i][j]);
					}
				}
			}
		}
		// sort neighbors based on utility + cost and move to the best possible one
		sort(neighbors.begin(), neighbors.end(), [](Cell& left, Cell& right) {
			return (left.get_utility() + left.get_cost()) < (right.get_utility() + right.get_cost());
		});
		// try each neighbor in order of utility increasing
		while (!neighbors.empty()) {
			//cout << "Best neighbor at (" << neighbors[0].get_loc_x() << ", " << neighbors[0].get_loc_y() << ") has utility + cost: " <<
			//	neighbors[0].get_utility() + neighbors[0].get_cost() << endl;
			//cout << "Pedestrian utility " << ped->get_utility() << endl;
			// move to the place of the best neighbor
			if (this->move_pedestrian(ped, neighbors[0])) {
				// if movement was successfull, break
				break;
			}
			neighbors.erase(neighbors.begin());
		}
	}
}

double Grid::get_euclidian_dist(Cell* c1, Cell* c2) {
	int x_diff{}, y_diff{};
	x_diff = c1->get_loc_x() - c2->get_loc_x();
	y_diff = c1->get_loc_y() - c2->get_loc_y();
	return sqrt(x_diff * x_diff + y_diff * y_diff);
}

void Grid::reset_utilities() {
	for(int i = 0; i < this->size; i++) {
		for(int j = 0; j < this->size; j++) {
			this->cells[i][j].set_utility(0);
		}
	}
}

void Grid::calculate_ped_interaction_cost() {
	for (Pedestrian* ped : this->pedestrians) {
		for (int i = 0; i < this->size; i++) {
			for (int j = 0; j < this->size; j++) {
				double dist = this->get_euclidian_dist(&this->cells[i][j], ped);
				double u = exp(1 / (dist * dist - 9));
				// r_max = 2
				if (dist < 3) {
					this->cells[i][j].set_cost(this->cells[i][j].get_cost() + u);
				}

				// add penalty to obstacles
				if (this->cells[i][j].get_cell_char() == Obstacle(0, 0, 0).get_cell_char()) {
					int big = pow(this->size, 3);
					this->cells[i][j].set_cost((double) big);
				}
			}
		}
	}
}

bool Grid::move_pedestrian(Cell *pedestrian, Cell neighbor) {
	// clear current pedestrian from grid
	this->empty_loc(pedestrian->get_loc_x(), pedestrian->get_loc_y(), pedestrian->get_size());
	// update pedestrian prev position
	pedestrian->update_prev_pos();
	// check if pedestrian can fit into neighbor cell
	if (!this->loc_is_empty(neighbor.get_loc_x(), neighbor.get_loc_y(), pedestrian->get_size())) {
		cout << "can't move pedestrian" << endl;
		this->set_pedestrian(pedestrian);
		return false;
	}
	else {
		// add pedestrian in new location
		// update pedestrian
		pedestrian->set_loc_x(neighbor.get_loc_x());
		pedestrian->set_loc_y(neighbor.get_loc_y());
	}
	// set pedestrian back to grid
	this->set_pedestrian(pedestrian);
	return true;
}

void Grid::set_pedestrian(Cell* pedestrian) {
	for (int i = pedestrian->get_loc_x() - pedestrian->get_size(); i <= pedestrian->get_loc_x() + pedestrian->get_size(); i++) {
		for (int j = pedestrian->get_loc_y() - pedestrian->get_size(); j <= pedestrian->get_loc_y() + pedestrian->get_size(); j++) {
			this->cells[i][j] = *pedestrian;
		}
	}
}

void Grid::evolve_dijkstra() {

}
