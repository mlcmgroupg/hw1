#pragma once
#include "Cell.h"
class Pedestrian :
	public Cell
{

private:
	int speed;
	int number_of_steps;

public:
	static int ped_count;
	Pedestrian(int, int, int, int, char);
};

