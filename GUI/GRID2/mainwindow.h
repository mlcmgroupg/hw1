#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Simulation.h"
#include <QLayout>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    int grid_size;
    int state;
    bool wait;
    void displayGrid(Simulation*);

private slots:
   void on_setGrid_button_pressed();

   void on_Task1_radio_pressed();

   void on_Task2_radio_pressed();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
