#include "mainwindow.h"
#include "ui_mainwindow.h"
//#include "Simulation.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->setGrid_button->hide();
}

void MainWindow::displayGrid(Simulation *s)
{
    QLayoutItem* item;
    while ( ( item = ui->baseGridLayout->takeAt( 0 ) ) != NULL )
    {
        delete item->widget();
        delete item;
    }

    ui->baseGridLayout->setSizeConstraint(QLayout::SetNoConstraint);

    for (int r = 0; r < grid_size; r++)
    {
       for (int c = 0; c < grid_size; c++)
       {
            char curr = s->grid->cells[r][c].get_cell_char();
            QLabel *label = new QLabel((QString)curr, this->ui->gridLayoutWidget);
            switch(curr)
            {
                case 'T':
                    label->setStyleSheet(
                    "background-color:black; color:white;font-size:12px; font-weight:bold; qproperty-alignment: AlignCenter");
                break;
                case 'O':
                    label->setStyleSheet(
                    "background-color:red; color:white; font-size:12px; font-weight:bold; qproperty-alignment: AlignCenter");
                break;
                case '-':
                     label->setStyleSheet(
                     "background-color:yellow; color:black; font-size:12px; font-weight:bold; qproperty-alignment: AlignCenter");
                break;
                default :
                     label->setStyleSheet(
                     "background-color:green; color:black; font-size:12px;font-weight:bold; qproperty-alignment: AlignCenter");
            }
            ui->baseGridLayout->addWidget(label, r, c);
       }
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_setGrid_button_pressed()
{
    Simulation *s = new Simulation(grid_size);
    if(state == 0)
    {
        state = 1;
        displayGrid(s);
        this->ui->setGrid_button->setText("Simulate");
    }
    else
    {
        state = 0;
        s->run();
        displayGrid(s);
        this->ui->setGrid_button->setText("Set Grid");
    }
    delete s;
}

void MainWindow::on_Task1_radio_pressed()
{
    state=0;
    this->ui->setGrid_button->setText("Set Grid");
    grid_size = 10;
    this->ui->setGrid_button->show();
}

void MainWindow::on_Task2_radio_pressed()
{
    state=0;
    this->ui->setGrid_button->setText("Set Grid");
    grid_size = 50;
    this->ui->setGrid_button->show();
}
