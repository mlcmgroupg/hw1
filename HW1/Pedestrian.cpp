#include "Pedestrian.h"

Pedestrian::Pedestrian(int loc_x, int loc_y, int size, int speed, char symb) :Cell(loc_x, loc_y, size) {
	this->cell_char = symb;
	this->speed = speed;
	this->number_of_steps = 0;
}

void Pedestrian::add_number_of_steps() {
	this->number_of_steps += this->size + 1;
}

int Pedestrian::get_number_of_steps() {
	return this->number_of_steps;
}