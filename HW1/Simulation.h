#pragma once
#include <iostream>
#include "Grid.h"

class Simulation
{
private:
	Grid *grid;

	Grid* init_grid();

public:
	Simulation();
	void run();
};

