#include "Cell.h"

Cell::Cell() {
	this->loc_x = -1;
	this->loc_y = -1;
	this->size = -1;
	this->prev_x = -1;
	this->prev_y = -1;
	this->cell_char = 'o';
	this->utility = 10000000;
	this->cost = 0;
}

Cell::Cell(int loc_x, int loc_y, int size) {
	this->loc_x = loc_x;
	this->loc_y = loc_y;
	this->size = size;
	this->prev_x = -1;
	this->prev_y = -1;
	this->cell_char = 'o';
	this->utility = 10000000;
	this->cost = 0;
}

char Cell::get_cell_char() {
	return this->cell_char;
}

int Cell::get_loc_x() {
	return this->loc_x;
}

void Cell::set_cost(double cost) {
	this->cost = cost;
}

double Cell::get_cost() {
	return this->cost;
}

int Cell::get_loc_y() {
	return this->loc_y;
}

int Cell::get_size() {
	return this->size;
}

int Cell::get_prev_x() {
	return this->prev_x;
}

int Cell::get_prev_y() {
	return this->prev_y;
}

void Cell::set_loc_x(int loc_x) {
	this->loc_x = loc_x;
}

void Cell::set_loc_y(int loc_y) {
	this->loc_y = loc_y;
}

void Cell::set_prev_x(int prev_x) {
	this->prev_x = prev_x;
}

void Cell::set_prev_y(int prev_y) {
	this->prev_y = prev_y;
}

double Cell::get_utility() {
	return this->utility;
}

void Cell::set_utility(double u) {
	this->utility = u;
}

void Cell::update_prev_pos() {
	this->prev_x = this->loc_x;
	this->prev_y = this->loc_y;
}

bool operator >(const Cell &c1, const Cell &c2) {
	return (c1.utility > c2.utility);
}