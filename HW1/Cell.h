#pragma once
class Cell
{
	
protected:
	char cell_char;
	int loc_x;
	int loc_y;
	int prev_x;
	int prev_y;
	int size;
	double utility;	// calc based on distance metric to target
	double cost;	// calc based on distance metric to other pedestrians

public:
	Cell();
	Cell(int, int, int);
	char get_cell_char();
	int get_loc_x();
	int get_loc_y();
	int get_prev_x();
	int get_prev_y();
	int get_size();
	void set_loc_x(int);
	void set_loc_y(int);
	void set_prev_x(int);
	void set_prev_y(int);
	double get_utility();
	void set_utility(double);
	void set_cost(double);
	double get_cost();
	void update_prev_pos();
	friend bool operator >(const Cell&, const Cell&);
};

