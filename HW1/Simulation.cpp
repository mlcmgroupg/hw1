#include "Simulation.h"

using namespace std;

Simulation::Simulation() {
	this->grid = this->init_grid();
	
}

Grid* Simulation::init_grid() {
	int count{ 0 }, ind{ 0 }, num{};
	int num_of_peds{}, num_of_obs{}, num_of_targets{};
	string fill_cell_types[3] = { "pedestrian", "obstacle", "target" };
	int loc_x{}, loc_y{}, obj_size{};

	cout << "Enter square size: ";
	cin >> num;
	Grid* grid = new Grid(num);
	grid->print_grid();

	// use case 1
	//grid->add_pedestrian(1, 1, 1, 1);
	//grid->add_pedestrian(0, 4, 0, 1);
	//grid->add_pedestrian(0, 8, 0, 1);
	//grid->add_target(8, 8, 1);
	// use case 2
	/*
	grid->add_pedestrian(24, 0, 0, 1);
	grid->add_obstacle(24, 4, 2);
	grid->add_obstacle(7, 6, 1);
	grid->add_pedestrian(0, 24, 0, 1);
	grid->add_pedestrian(49, 24, 0, 1);
	grid->add_obstacle(47, 24, 1);
	grid->add_pedestrian(24, 49, 0, 1);
	grid->add_pedestrian(2, 2, 1, 1);
	grid->add_target(24, 24, 1);
	grid->add_target(30, 30, 1);
	cout << endl;
	
	grid->print_grid();
	*/

	
	
	while (ind < 3) {
		cout << endl << "Enter number of " << fill_cell_types[ind] << "s: ";
		cin >> num;
		if (num > ((grid->get_size() - 1) * (grid->get_size() - 1))) {
			cout << "The grid is too small for this many items!" << endl;
			continue;
		}
		if (num < 0) {
			cout << "Too small!" << endl;
			continue;
		}
		count = 0;
		while (count < num) {
			// get size
			cout << endl << "Enter size of " << fill_cell_types[ind] << " " << count + 1 << ": ";
			cin >> obj_size;
			if (obj_size > grid->get_size() - 1) {
				cout << "The object is too big!" << endl;
				continue;
			}
			if (obj_size < 0) {
				cout << "the object is too small!" << endl;
				continue;
			}

			// get location x
			cout << "Enter " << fill_cell_types[ind] << " " << count + 1 << " location x: ";
			cin >> loc_x;
			if ((loc_x - obj_size) < 0 || (loc_x + obj_size) > (grid->get_size() - 1)) {
				cout << "That position is outside of grid!" << endl;
				continue;
			}

			// get location y
			cout << "Enter " << fill_cell_types[ind] << " " << count + 1 << " location y: ";
			cin >> loc_y;
			if ((loc_y - obj_size) < 0 || (loc_y + obj_size) >(grid->get_size() - 1)) {
				cout << "That position is outside of grid!" << endl;
				continue;
			}

			// create if possible
			if (grid->loc_is_empty(loc_x, loc_y, obj_size)) {
				grid->add_cell(fill_cell_types[ind], loc_x, loc_y, obj_size);
				count++;
				grid->print_grid();
			}
			else {
				cout << "That position is already taken!" << endl;
			}
		}
		ind++;
	}

	return grid;
}

void Simulation::run() {
	bool game_over = false;
	/*
	while (!game_over) {
		this->grid->evolve_euclid();
		break;
	}
	this->grid->evolve_euclid();
	this->grid->evolve_euclid();
	this->grid->evolve_euclid();
	this->grid->evolve_euclid();
	this->grid->evolve_euclid();
	*/

	for (int i = 0; i < 30; i++) {
		this->grid->evolve_euclid();
		// this->grid->print_utility_grid();
		if (i % 5 == 0) {
			cout << endl << endl << "Step " << i << endl;
			this->grid->print_grid();
			cout << endl;
		}
	}
	cout << endl << endl << "Final grid status" << endl;
	this->grid->print_grid();
}

